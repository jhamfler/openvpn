# Setup Client / Server
Run this on the client and on the server.
```
# as normal user
git clone https://gitlab.com/jhamfler/openvpn.git
cd openvpn
dir=$(mktemp -d)
cp -R . $dir
```
```
# as root
sudo apt-get install openvpn -y
sudo openssl genpkey -algorithm ed25519 -out /etc/openvpn/key.pem # generate private key
sudo openssl pkey -in /etc/openvpn/key.pem -pubout -out /etc/openvpn/pub.pem # extract public key

# server
sudo openssl dhparam -out /etc/openvpn/dh2048.pem 2048
sudo openssl req -new -key /etc/openvpn/key.pem -out /etc/openvpn/server1csr.pem # generate certificate signing request
scp /etc/openvpn/server1csr.pem root@192.168.178.108:/var/ # move file to CA (not recomended this way)

# client
sudo openssl req -new -key /etc/openvpn/key.pem -out /etc/openvpn/client1csr.pem # generate certificate signing request
scp /etc/openvpn/client1csr.pem root@192.168.178.108:/var/ # move file to CA (not recomended this way)
```

# Setup CA
Run the following commands on your CA.
```
# as normal user
git clone https://gitlab.com/jhamfler/openvpn.git
cd openvpn
dir=$(mktemp -d)
cp -R . $dir
```

Please change the IP addresses to fit your needs.
Open a root shell (e.g. `sudo bash`) and run these commands.

If you are being asked to provide information you can simply hit enter until it is finished.
If you are being asked yes or no questions just hit `y` and then `enter`. This is neccessary to sign your certificates.
```
# as root
client1=192.168.178.110     # this is the (current) ip of the vpn client
server1=192.168.178.109    # this is the ip of the vpn server
apt-get install openvpn easy-rsa -y
mkdir -p /etc/openvpn/easy-rsa
cp -R /usr/share/easy-rsa/* /etc/openvpn/easy-rsa/
cd /etc/openvpn/easy-rsa/

./easyrsa init-pki # build directory structure end everything
./easyrsa build-ca # build ca keys

./easyrsa import-req /var/server1csr.pem vpnserver1 # importiere CSR
./easyrsa sign-req server vpnserver1 # signiere CSR für den server
scp /etc/openvpn/easy-rsa/pki/ca.crt root@"$server1":/etc/openvpn/ca.crt # copy CA certificate to the server
scp /etc/openvpn/easy-rsa/pki/issued/vpnserver1.crt root@"$server1":/etc/openvpn/crt.pem # move certificate to vpnserver

./easyrsa import-req /var/client1csr.pem vpnclient1 # importiere CSR
./easyrsa sign-req client vpnclient1 # signiere CSR für den client
scp /etc/openvpn/easy-rsa/pki/ca.crt root@"$client1":/etc/openvpn/ca.crt # copy CA certificate to the client
scp /etc/openvpn/easy-rsa/pki/issued/vpnclient1.crt root@"$client1":/etc/openvpn/crt.pem # move certificate to vpnclient
```

# Test
Run the tests as root.
### Server side first
```
date # check the time
sudo openvpn --dev tun1 --ifconfig 10.9.8.1 10.9.8.2 --tls-server --dh /etc/openvpn/dh2048.pem --ca /etc/openvpn/ca.crt --cert /etc/openvpn/crt.pem --key /etc/openvpn/key.pem --reneg-sec 60 --verb 5 --port 465 --proto tcp-server --askpass
```

### Client side afterwards
```
date # check if the time is equal to the server time
sudo openvpn --remote 192.168.178.109 --dev tun1 --ifconfig 10.9.8.1 10.9.8.2 --tls-client --ca /etc/openvpn/ca.crt --cert /etc/openvpn/crt.pem --key /etc/openvpn/key.pem --reneg-sec 60 --verb 5 --port 465 --proto tcp-client --askpass
openvpn --remote SERVER_IP --dev tun1 --ifconfig 10.9.8.2 10.9.8.1 --tls-client --ca /etc/openvpn/easy-rsa/keys/ca.crt --cert /etc/openvpn/easy-rsa/keys/ab1.crt --key /etc/openvpn/easy-rsa/keys/ab1.key --reneg-sec 60 --verb 5 --port 465 --proto tcp-client
```

If the client and server time differs from each other you need to synchronise it. This can be neccessary when your device has no real time clock. The RaspberryPi for example has a wrong `date` each time it is booted. Use ntp to accomplish that. When using Debian you can do that with the following commands.
```
# as root
apt-get install ntp    # install ntp
service ntp stop       # stop the daemon
ntpd -qg               # update time
service ntp start      # start the daemon
systemctl enable ntp   # enable it on boot
```

# Final Configuration
## Server Config
```
# as root
cp $dir/server.conf /etc/openvpn/server.conf
mkdir -p /etc/openvpn/log/
systemctl daemon-reload      # reload all deamons
/etc/init.d/openvpn restart  # always
tail -n 30 /var/log/syslog   # check for errors
ifconfig                     # check if tun is up
```

## Client Config
```
sudo cp $dir/client.conf /etc/openvpn/client.conf
sudo systemctl daemon-reload
sudo /etc/init.d/openvpn restart
sudo tail -n 30 /var/log/syslog   # check for errors
ip l                     # check if tun is up
```
